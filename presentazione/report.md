# Testare applicazioni web

Testare un'applicazione web richiede una varietà di test infatti sono necessari:

* Test di unità
* Test di integrazione
* Test di carico
* Test di sicurezza
* Test di accettazione
* Test di interfaccia
* Test di compatibilità

Inoltre un'applicazione web fa uso intensivo di database e di servizi REST propri e di terze parti, che complicano il processo di testing. 

Qui si discuterà solo di test di accettazione, \(UAT in breve\), e di come renderli automatici tramite Selenium WebDriver.

# UAT in applicazioni web

Gli UAT sono i test di accettazione concordati con il committente e descritti tramite le user story, che indicano quando il prodotto rispetta le aspettative del commitente. Non solo: questi test ci aiutano anche a verificare se siano state inserite delle regressioni, riguardo sempre alle aspettative del commitente. Quindi risulta  utile eseguire questi test durante lo sviluppo, piuttosto che eseguirli solo con il committente o nelle fasi finali di produzione\sprint. 

Fare gli UAT manualmente  vuol dire avviare l'applicazione riempire un certo numero di form e verificare che tutto funzioni per bene. Da questa breve descrizione si capisce che tale operazione richiede molto tempo e soprattutto è caratterizzata dalla monotonia delle azioni, che apre la strada ad esecuzioni scorrette dei test rendendoli inconsistenti ed inutili.

È possibile rendere questi test automatici e meno _error prone_? La risposta è affermativa, ed abbiamo due possibilità:

1. Tool di record & playback 
2. Test automatici

I test eseguiti con i tool di record & playback, in generale, hanno il problema di risultare poco manutenibili e rendono più difficoltoso eventuali verifiche aggiuntive e/o vari tweak, oltre che rendere difficile l'esecuzione di data driven test. Inolte gli script generati sono poco leggibili e di difficile manutenzione, quindi se l'interfaccia cambia spesso rischiamo di dover registrare nuovamente i test. Possono risultare utili per una prototipazione rapida dei test.

Noi ci concentreremo sui test automatici con l'ausilio di Selenium WebDriver, un tool che permette di instrumentare i browser e di emulare le interazioni dell'utente.

# Selenium WebDriver
[Git Hub](https://github.com/SeleniumHQ/selenium)<br/>
[Selenium website](http://www.seleniumhq.org/) <br/>
[Java doc](https://seleniumhq.github.io/selenium/docs/api/java/index.html)<br/>

Selenium WebDriver nasce dall’unione di due tool: Selenium e Webdriver.

Selenium è nato nel 2004 come libreria javascript per automatizzare test di interfaccia ed è stato sviluppato da Jason Huggins, un ingengere di ThoughtWorks. Google utilizzava Selenium per testare le sue applicazioni, ma i tester dovevano trovare dei workaround per superare le limitazioni di Selenium, dato che i browser applicano varie restrizioni ai file javascript. Per questo nel 2006 nasce il progetto WebDriver creato da Simon Stewart, un ingegnere di Google, per risolvere i punti critici di Selenium e per poter permettere l’instrumentazione programmatica dei browser in modo da emulare l'interazione utente.

Nel 2008 i due progetti si uniscono per dare vita a Selenium Webdriver. Oggi Selenium  implementa le proprie API in molti linguaggi \(java, python, C\# ecc…\) ed ha ampliato il suo ecosistema:

* Selenium IDE: è un add-on di firefox per poter fare record/playback test.

* Selenium Grid: è un tool per poter avviare i test in diversi browser e diversi ambienti in parallelo.

In poche parole Selenium 2 ci permette di scrivere i test nel nostro linguaggio di programmazione preferito e di automatizzare le azioni da testare. È possibile utilizzarlo con diversi framework di testing, come Junit o Cucumber, ed è possibile integrarlo con i serivzi di CI.
### Come funziona
![Local Image](./selenium_graph.jpg) <br/>

Selenium WebDriver per automatizzare i test ha bisogno che i browser abbiano un supporto per questo tipo di operazione fornendo i driver che permettono l’instrumentazione del browser implementando le API specificate da Selenium WD. È stato fatto anche uno standard W3C per sviluppare i web driver, accessibile a questo collegamento: [https://www.w3.org/TR/webdriver/](https://www.w3.org/TR/webdriver/). Le API dei webdriver permettono di accedere e manipolare il DOM e di controllare il browser emulando l'interazione utente rendendo possibile l'instrumentazione automatica dei browser.

# Tutorial

## Preparazione ambiente e primo test

Prima di poter cominciare a sviluppare i nostri test dobbiamo preparare l'ambiente. I test verranno eseguiti con google chrome, quindi dobbiamo scaricare chrome driver da qui: [https://chromedriver.storage.googleapis.com/index.html?path=2.34/](https://chromedriver.storage.googleapis.com/index.html?path=2.34/)

Una volta scaricato il driver possiamo inserirlo nel path/variabili d'ambiente per renderlo visibile a tutto il sistema.  Per testare se abbiamo fatto le cose correttamente si può aprire il terminale ed eseguire il comando

```bash
chromedriver -v
```
Per poterlo utilizzare nei test Java si deve aggiungere la seguente dipendenza sul file build.gradle:
```groovy
    'org.seleniumhq.selenium:selenium-java:3.8.1'
```
Testiamo che tutto funzioni con JUnit. Per avviare il browser bisogna creare una istanza della classe ```ChromeDriver```.


```java
    @Test
    void firstSeleniumTest(){
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.google.it/");
        assertThat(driver.getTitle()).isEqualToIgnoringCase("google");
        driver.close();
    }
```



## Trattare elementi dinamici

Per poter gestire gli elementi dinamici Selenium fornisce due diverse modalità di attesa.

### Attesa implicita

Per implementare una attesa implicita si deve chiamare il metodo ```implicitlyWait``` sull'istanza del driver. Questo metodo fa scattare un polling sul DOM quando viene ricercato un elemento ma non viene trovato subito.

```java
    @Test
    void implicitWaitTest(){
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://alessio.cuzzocrea.gitlab.io/plain-html/");
        WebElement button = driver.findElement(By.id("implicit-wait-button"));
        button.click();
    }
```

Le attese implicite non permettono di gestire agevolmente i casi in cui l'elemento è presente nel DOM ma non è visibile. 

### Attesa esplicita
Attesa esplicita vuol dire che per ogni elemento che cerchiamo nel DOM impostiamo un timeout e delle condizioni di successo. Per implementarla bisogna istanziare la classe FluentWait e specificare l'oggetto sul quale attendere.
```java
    @Test
    void explicitWaitTest(){
        FluentWait<WebDriver> wait = new FluentWait<>(driver);
        wait.withTimeout(3,TimeUnit.SECONDS);
        WebElement button = wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.id("explicit-wait-button"))
        );
        button.click();
    }
```

Per cominciare l'attesa bisogna chiamare il metodo `until` che prende in ingresso una funzione che verrà ripetuta fino a quando:

* la funzione restituisce un valore diverso da null o false
* la funzione lancia un'eccezione non ignorata esplicitamente tramite il metodo fluent ```ignoring```
* il timeout scade

Utilizzando FluentWait è possibile impostare un'attesa su ogni elemento del DOM, il che torna utile, a titolo di esempio, se vogliamo verificare che ad un certo elemento del DOM sia stata aggiunta una classe CSS.

Nell'esempio mostrato sopra viene utilizzata la classe `ExpectedConditions` che contiene delle funzioni preconfenzionate per i casi d'uso più comuni, qui trovate la documentazione [ExpectedConditions](https://seleniumhq.github.io/selenium/docs/api/java/index.html?org/openqa/selenium/support/ui/ExpectedConditions.html).

Esiste anche la classe WebDriverWait, che è una specializzazione di FluentWait che opera solo sugli oggetti WebDriver.

### Sviluppo di  User Acceptance Test

Supponiamo che ci è stato commisionato lo sviluppo di un'applicazione web che permetta di tener traccia degli orari nelle diverse città del mondo. I requisiti, in maniera generica, sono:

* La ricerca di città
* Una lista delle città preferite con relativo orario
* Conversione di orario tra varie città 

La webapp è accessibile al seguente indirizzo: http://alessio.cuzzocrea.gitlab.io/orologio-mondiale-webapp
Decidiamo di sviluppare la user story che segue:

```
Come un utente con un sacco di amici nel mondo
Voglio poter cercare delle città ed aggiungerle ai favoriti
In modo che siano visibili rapidamente in una lista
```

```
        Given that I want to know the time in the cities
  	      |Milano|
        When I add them to my favorites
        Then They should be visible in the favorite list
```

Innanzitutto dobbiamo capire quante pagine sono coinvolte e quali elementi dobbiamo usare per eseguire il test:

1. Abbiamo bisogno di mettere dell'input nella searchbox
2. Clickare la città corretta
3. Navigare verso "Lista città"
4. Assicurarsi che la città inserita sia presente

Occupiamoci della searchbox. Per inserire del testo nella searchbox dobbiamo interrogare il DOM per localizzarla:
```java
WebElement searchbox = driver.findElement(By.tagName("input"));
```
Adesso possiamo inserire il testo:
```
searchbox.sendKeys(city);
```
Adesso dobbiamo clickare sulla prima città che viene fuori dai risultati. Per farlo dobbiamo aspettare che l'applicazione carichi i risultati e li mostri. Prepariamo l'oggetto WebDriverWait

```java
    FluentWait<WebDriver> wait = new FluentWait<>(driver).withTimeout(3, TimeUnit.SECONDS);
```
e lo utilizziamo così
```java
    WebElement result = wait
      .ignoring(StaleElementReferenceException.class)
      .until(webDriver -> {
            List<WebElement> results = webDriver.findElements(By.xpath(citiesResultXPath));
            Optional<WebElement> element = results
                                            .stream()
                                            .filter(we -> 
                                               we.getText().contains(city))
                                            .findFirst();
            return element.orElse(null);
            });
```
dove la variabile citiesResultXPath rappresenta la stringa:
```java
    citiesResultXPath="//*/ul[@class='list-group']//li"
```
che mi permette di trovare la lista dei risultati.
All'interno di ```until``` la funzione cercherà un WebElement che contiene il nome della città cercata.
Una volta trovato non ci resta che cliccarlo. 
```java
result.click();
```
Per assicurarsi che la città venga effettivamente aggiunta dobbiamo inserire una attesa sul feedback del risultato, in questo bisognoa istanziare ```FluentWait``` sull'elemento appena clickato e aspettare che venga aggiunta una classe CSS.
```java
    FluentWait<WebElement> waitElement = new FluentWait<>(result)
        .withTimeout(4, TimeUnit.SECONDS);
    waitElement.until(webElement -> 
        webElement.getAttribute("class").contains("info-background-color"));
```
Procediamo con la verifica del risultato. Navigando alla pagina della lista città dobbiamo trovare un elemento che rappresenti la città appena inserita.
```java
List<WebElement> favoriteCities = wait.until(webDriver ->  {
            List<WebElement> elements = driver.findElements(By.xpath(favoriteCitiesXPath));
            return elements.size() == 0 ? null : elements;
        });

        Assertions.assertThat(favoriteCities).extracting(WebElement::getText).containsSequence(city);
```

## Test dockerizzati e CI
È possibile eseguire i test in docker a patto che i browser scelti per i test possono essere eseguiti in modalità headless, cioè senza interfaccia grafica.
 Ad esempio avviamo chrome in headless-mode

 ```java
    ChromeOptiopns opt = new ChromeOptions();
    opt.addArguments("no-sandbox", "headless");
    driver = new ChromeDriver(opt);
 ```
 L'opzione ```no-sandbox``` è necessaria per poter eseguire chrome in modalità headless in un ambiente docker, dato che sono richiesti dei permessi che docker di default non fornisce. Dato che è possibile eseguire i test in docker, possiamo anche integrarli in un ambiente di CI senza troppe difficoltà. Nel repository del tutorial trovate il Dockerfile e la configurazione per la CI con gitlab.

 # Linea guida di sviluppo con Selenium
 ## UI Mapping e localizzazione elementi

Una buona pratica per non riempire il codice di test di stringhe "magiche" per localizzare gli elementi del DOM  consiste nell'utilizzare un file ```.properties``` dal quale leggere queste stringhe. Così facendo si migliora la leggibilità del codice, soprattutto per chi non conosce la struttura della pagina. Inoltre tutti i membri del team possono fare delle modifiche all'interfaccia, ad esempio cambiare un id,  senza dover ricompilare il codice per far funzionare i test. <br />
    Come strategia di localizzazione elementi è consigliato utilizzare gli ID quando possibile dato che il browser riesce a trovarli più velocemente.

 ## 
 ## Page Object Pattern
 > _If you have WebDriver APIs in your test methods, You're Doing It Wrong._ <br/>
 > _Simon Stewart_

[Articolo](https://martinfowler.com/bliki/PageObject.html) di Martin Fowler.<br/>

Il codice di test **non** deve contenere chiamate alle API WebDriver, questo perchè:
* Dobbiamo separare il _come_ accediamo e gestiamo le pagine web e la verifica dei comportamenti dell'applicazione.
* Non duplicazione di codice
* Maggiore manutenibilità e leggibilità dei test

In pratica dobbiamo costruire delle classi, _Page Object_ che ci permettono di accedere agli elementi da testare sulla pagina in modo che nei test abbiamo un codice pulito e astratto dai riferimenti HTML e di WebDriver. Inoltre è importante sottolineare che non vanno fatte asserzioni nei Page Object, al più delle verifiche.

# Considerazioni finali
Testare le applicazioni web con Selenium non è triviale, anche dal tutorial appena mostrato si nota una certa complessità nell'eseguire dei test relativamente semplici.
È uno strumento molto utile senza ombra di dubbio ma bisogna sempre capire quando è il caso di utilizzarlo o meno.