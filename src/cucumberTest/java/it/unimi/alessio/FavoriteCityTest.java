package it.unimi.alessio;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import it.unimi.alessio.pages.FavoriteCityPage;
import it.unimi.alessio.pages.Page;
import it.unimi.alessio.pages.SearchCitiesPage;
import org.assertj.core.api.Assertions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.IOException;
import java.util.List;

import static it.unimi.alessio.UIMapper.getProperty;


public class FavoriteCityTest {
    private List<String> cities;
    private WebDriver driver;
    private Page currentPage;
    @Before
    public void setupBrowser() throws IOException {
        ChromeOptions opt = new ChromeOptions();
        opt.addArguments("window-size=1920,1080");
        if(System.getenv().containsKey("DOCKERIZED")){
            opt.addArguments("no-sandbox", "headless");

        }
        driver = new ChromeDriver(opt);
        driver.get(getProperty("baseUrl"));
    }

    @Given("^I want to know the time in the cities$")
    public void iWantToKnowTheTimeInTheCities(List<String> cities) {
        this.cities = cities;
    }

    @When("^I add them to my favorites$")
    public void iAddThemToMyFavorites() throws IOException {
        currentPage = new SearchCitiesPage(driver).searchAndFavorite(cities);
    }

    @Then("^They should appear to the favorite list$")
    public void theyShouldAppearToTheFavoriteList() throws IOException {
        currentPage = currentPage.goToFavoriteCities();
        List<String> favoriteCities = ((FavoriteCityPage)currentPage).getFavoriteCities();
        Assertions.assertThat(favoriteCities).isEqualTo(cities);
    }


    @After
    public void closeBrowser(){
        driver.close();
    }
}
