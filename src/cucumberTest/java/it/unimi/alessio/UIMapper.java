package it.unimi.alessio;


import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class UIMapper {


    public static String getProperty(String propertyName) throws IOException {
        String result;
        InputStream inputStream;
        String propFileName = "ui_mapping.properties";
        inputStream = UIMapper.class.getClassLoader().getResourceAsStream(propFileName);
        if (inputStream == null) {
            throw new FileNotFoundException("filename: " + propFileName);
        }
        Properties prop = new Properties();
        prop.load(inputStream);
        result = prop.getProperty(propertyName);
        return result;
    }
}