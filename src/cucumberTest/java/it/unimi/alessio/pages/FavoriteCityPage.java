package it.unimi.alessio.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static it.unimi.alessio.UIMapper.getProperty;
import static it.unimi.alessio.pages.FluentWaitFactory.buildFluentWait;

public class FavoriteCityPage implements Page{
    private WebDriver driver;
    public FavoriteCityPage(WebDriver driver) {
        this.driver = driver;
    }


    public List<String> getFavoriteCities() throws IOException {
        String citiesListXPath = getProperty("favoriteCitiesListXPath");
        FluentWait<WebDriver> wait = buildFluentWait(driver);
        List<WebElement> favoriteCities = wait.until(
                webDriver -> webDriver.findElements(By.xpath(citiesListXPath))
        );
        return favoriteCities.stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());
    }

    @Override
    public Page goToFavoriteCities() throws IOException {
        return this;
    }
}
