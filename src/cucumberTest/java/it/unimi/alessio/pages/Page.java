package it.unimi.alessio.pages;

import java.io.IOException;

public interface Page {
    Page goToFavoriteCities() throws IOException;
}
