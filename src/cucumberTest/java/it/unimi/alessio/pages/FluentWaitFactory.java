package it.unimi.alessio.pages;

import org.openqa.selenium.support.ui.FluentWait;

import java.util.concurrent.TimeUnit;

public class FluentWaitFactory {
    public static <T> FluentWait<T> buildFluentWait(T waitObject){
        return new FluentWait<>(waitObject).withTimeout(5, TimeUnit.SECONDS);
    }
}
