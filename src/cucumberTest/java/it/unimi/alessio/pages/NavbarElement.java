package it.unimi.alessio.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static it.unimi.alessio.UIMapper.getProperty;
import static it.unimi.alessio.pages.FluentWaitFactory.buildFluentWait;

public class NavbarElement {
    private final WebDriver driver;
    private final WebElement favoriteCitiesButton;
    private FluentWait<WebDriver> wait;
    public NavbarElement(WebDriver driver) throws IOException {
        this.driver = driver;
        String favoriteCitiesXPath = getProperty("favoriteCitiesButtonXPath");
        wait = buildFluentWait(driver);
        favoriteCitiesButton = wait.until(
                webDriver -> webDriver.findElement(By.xpath(favoriteCitiesXPath))
        );
    }
    public Page goToFavoriteCities() throws IOException {
//      check in caso il proiettore durante la presentazione non supporti la giusta risoluzione
        checkBurgerMenuPresence();
        favoriteCitiesButton.click();
        wait.ignoring(NoSuchElementException.class).until(
                webDriver -> webDriver.findElement(By.xpath("//*[@name=\"cities-list\"]"))
        );
        return new FavoriteCityPage(driver);
    }

    private void checkBurgerMenuPresence() throws IOException {
        String burgerMenuId = getProperty("burgerMenuId");
        WebElement burgerMenu = driver.findElement(By.id(burgerMenuId));
        if(burgerMenu.isDisplayed()){
            burgerMenu.click();
            wait.until(ExpectedConditions.visibilityOf(favoriteCitiesButton));
        }
    }
}
