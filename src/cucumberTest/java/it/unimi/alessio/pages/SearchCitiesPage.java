package it.unimi.alessio.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.FluentWait;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import static it.unimi.alessio.UIMapper.getProperty;
import static it.unimi.alessio.pages.FluentWaitFactory.buildFluentWait;

public class SearchCitiesPage implements Page {
    private WebDriver driver;
    private WebElement searchBox;
    private WebElement favoriteCitiesButton;

    public SearchCitiesPage(WebDriver driver) throws IOException {
        this.driver = driver;
        String favoritCitiesXPath = getProperty("favoriteCitiesButtonXPath");
        FluentWait<WebDriver> wait = buildFluentWait(driver);
        searchBox = wait.ignoring(NoSuchElementException.class).until(
                webDriver -> webDriver.findElement(By.tagName("input"))
        );
        favoriteCitiesButton = wait.ignoring(NoSuchElementException.class).until(
                webDriver -> webDriver.findElement(By.xpath(favoritCitiesXPath))
        );
    }

    public Page searchAndFavorite(List<String> cities) throws IOException {
        FluentWait<WebDriver> wait = buildFluentWait(driver);
        WebElement result;
        String citiesResultXPath = getProperty("citiesResultXPath");
        for (String city : cities) {
            searchBox.clear();
            searchBox.sendKeys(city);
            result = wait
                    .ignoring(StaleElementReferenceException.class)
                    .until(webDriver -> {
                        List<WebElement> results = webDriver.findElements(By.xpath(citiesResultXPath));
                        Optional<WebElement> element = findCity(city, results);
                        return element.orElse(null);
            });
            result.click();
            FluentWait<WebElement> waitElement = buildFluentWait(result);
            waitElement.until(webElement -> webElement.getAttribute("class").contains("info-background-color"));
        }
        return this;
    }

    private Optional<WebElement> findCity(String city, List<WebElement> results) {
        return results.stream().filter(
                we -> we.getText().contains(city)
        ).findFirst();
    }


    @Override
    public Page goToFavoriteCities() throws IOException {
        NavbarElement navbarElement = new NavbarElement(driver);
        return navbarElement.goToFavoriteCities();
    }
}