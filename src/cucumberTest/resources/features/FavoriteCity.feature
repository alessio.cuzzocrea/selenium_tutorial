Feature: Favoriting city
	  As a person with a lot of friends in the world
  	  In order to know the time in different cities
  	  I want to have a list with the current time for each city

      Scenario: Adding cities
        Given I want to know the time in the cities
  	      |Milano|
  	      |Londra|
        When I add them to my favorites
        Then They should appear to the favorite list