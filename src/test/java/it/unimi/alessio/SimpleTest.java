package it.unimi.alessio;


import static org.assertj.core.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;


import java.io.IOException;

public class SimpleTest {

    @Test
    void firstSeleniumTest(){
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.google.it/");
        assertThat(driver.getTitle()).isEqualToIgnoringCase("google");
        driver.close();
    }
}
