package it.unimi.alessio;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class SeleniumWaitsTest {
    private WebDriver driver;

    @BeforeEach
    void setupBrowser(){
        driver = new ChromeDriver();
        driver.get("http://alessio.cuzzocrea.gitlab.io/plain-html/");
    }
    @AfterEach
    void closeBrowser(){
        driver.close();
    }
    @Test
    void implicitWaitTest(){
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebElement button = driver.findElement(By.id("implicit-wait-button"));
        button.click();
    }
    @Test
    void explicitWaitTest(){
        FluentWait<WebDriver> wait = new FluentWait<>(driver);
        wait.withTimeout(3,TimeUnit.SECONDS);
        WebElement button = wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.id("explicit-wait-button"))
        );
        button.click();
    }
}
