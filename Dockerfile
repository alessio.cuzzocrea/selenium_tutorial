#per costruire e pushare l'immagine:
# docker login
# docker image build -t MY_DOCKER_ID/selenium .
# docker push MY_DOCKER_ID/selenium


#per usare interattivamente l'immagine
# docker run -it --name SOME_NAME --mount type=bind,source="$(pwd)",target=/src/app MY_DOCKER_ID/selenium /bin/bash
# se si vuole avviare il container una sola volta eseguendo i test invece aggiungere alla fine del file:

# WORKDIR /src/app
# COPY . .
# CMD ["gradle", "cucumberTest"]

#e dare il comando
# docker container run --rm MY_DOCKER_ID/selenium
#col secondo approccio bisogna però ripetere il processo di build prima di avviare il container

FROM java:8-jdk

RUN echo "deb http://dl.google.com/linux/chrome/deb/ stable main" | tee -a /etc/apt/sources.list &&  wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN apt-get update
RUN apt-get -y install libxpm4 libxrender1 libgtk2.0-0 libnss3 libgconf-2-4
RUN apt-get -y install google-chrome-stable

RUN wget -c https://services.gradle.org/distributions/gradle-4.4.1-bin.zip && mkdir /opt/gradle
RUN unzip -d /opt/gradle gradle-4.4.1-bin.zip && ls /opt/gradle/gradle-4.4.1
ENV PATH="/opt/gradle/gradle-4.4.1/bin:${PATH}"
ENV DOCKERIZED="DOCKERIZED"
RUN wget  https://chromedriver.storage.googleapis.com/2.34/chromedriver_linux64.zip
RUN unzip chromedriver_linux64.zip -d /usr/local/bin
RUN chmod +x /usr/local/bin/chromedriver
RUN mkdir -p /src/app